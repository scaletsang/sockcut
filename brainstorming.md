In a REST api, I often found a big hassle to rewrite my switch statement of matching incoming message types
from the server or the client, it is my take to write a message type checker and dynamically call the corresponding
message handler method:
1. Server: Pack data into json, and send to client
2. Client: receives a json and initiate instance from the corresponding message type class, the data is then handle by the instance method.
3. Client: Pack data into json, and send to server
4. Server: receives a json and initiate instance from the corresponding message type class, the data is then handle by the instance method.

It seems that in both the client side and the server side, it exsists the same process:
user_actions/files -> data,
data -> json, 
json -> class_instance, 
class_instance.class_method() -> side effects

Whenever the message protocol is changed due to new features or new versions of some mechanics,
I have to rewrite/ write 4 things for one additional message type:
1. Client side: pack data in the corresponding json structure
2. Server side: listen and unpack to that particular json structure
3. Server side: pack data in the corresponding json structure
4. Client side: listen and unpack to that particular json structure

This is why I propose the idea of using a machine readable message protocol that is written in valid json format
to tell the client and server how to pack the data in the right json format, and on receive, expose a class instance of the 
corresponding message type that the receiver can work with.

API:
```haskell
type types_vector = Vector<type_name, message_class>
generate_types_vector:: json_protocol -> types_vector
pack_data:: data -> message_type -> types_vector -> json
on_receive:: message_type -> json_protocol -> (message_type_class_instance -> side_effects)
```

json_protocol determines the structure of different json message types,
hence it is used to validate incoming message.

type_vector is a hash of type classes, each class is generated from the json_protocol.
Each type class is used to pack data into json, and make side effects on class method call.

## More ideas

Looking into GraphQL, it seem that it is what I want but not exactly how I want it to be. It still have too many layers of complexity to use. 

But it lets me think about the possibility to have a function type which is an alias of the object type, like this:

```javascript
  {
    'field': {'functionName(param1,param2)': {
      'param1': 'str',
      'param2': 'bool',
      'return': 'str'
    }}
  }
```

## Difference between my thing and GraphQL

Difference between a query and a shared message protocol system
GraphQL: 
client: make query -> server: receive and run query -> server: send query result -> client: receive query result
MsgProtocol: 
client: send message -> server: receive and run functions based on the message type -> server: perform side effects
server: send message -> client: receive and run functions based on the message type -> client: perform side effects

## Best Practice

One Protocol for any arbitary types
One Server protocol for message types that server send out
One Client protocol for message types that the client send out