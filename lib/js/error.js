class RethrownError extends Error {
  constructor(message, data, type, error) {
    if (error) message = `[backtraced] ${message}`
    super(message)
    this.name = this.constructor.name
    const stack = this.stack.split('\n')
    const messageLines =  (this.message.match(/\n/g)||[]).length + 2
    const stackMessage = stack.slice(0, messageLines)
    if (error) {
      this.stack = [error.stack].concat(stackMessage).join('\n')
    } else {
      const restStackMessage = stack.splice(messageLines)
      let errorObjArr = []
      if (type) errorObjArr.push('    Expected type:', generatePrettyErrorObj(type))
      errorObjArr.push('    Received Data:', generatePrettyErrorObj(data))
      this.stack = [ ...stackMessage, ...errorObjArr, ...restStackMessage ].join('\n')
    }
  }
}

///////////////////////////
//////    Errors    ///////
///////////////////////////

export class GenericTypeError extends RethrownError {
  constructor(data, typeName, type, error = null) {
    const message = `'${typeName}' is expected to be a generic type of the following:`
    super(message, data, type, error)
  }
}

export class ConstantTypeError extends RethrownError {
  constructor(data, typeName, val, error = null) {
    const message = `'${typeName}' is expected to be a constant value of ${val}.`
    super(message, data, null, error)
  }
}

export class UnionTypeError extends RethrownError {
  constructor(data, typeName, types, error = null) {
    const message = `UnmatchUnion: '${typeName}' is expected to be either one of the following types: ${types}. Error data content: ${data}.`
    super(message, data, types, error)
  }
}

export class ArrayTypeError extends RethrownError {
  constructor(errorArrayItem, arrayName, type, itemIndex, error = null) {
    const message = `Array '${arrayName}' is expected to be an array with the following type. Error occured at index ${itemIndex}.`
    super(message, errorArrayItem, type, error)
  }
}

export class UnmatchArrayLengthError extends RethrownError {
  constructor(arrayData, arrayName, length, error = null) {
    const message = `UnmatchArrayLength: ${arrayName} is expected to have a length of ${length}. Error array content: ${arrayData}.`
    super(message, arrayData, null, error)
  }
}

export class ObjectTypeError extends RethrownError {
  constructor(objectData, objectName, type , error = null) {
    const message = `The object '${objectName}' is expected to have the following type:`
    super(message, objectData, type, error)
  }
}

export class MissingObjectPropertyError extends RethrownError {
  constructor(objectName, propType, error = null) {
    const message = `${objectName} object missing the property ${propType}.`
    super(message, objectData, propType, error)
  }
}

///////////////////////////////
/// Passing errored object  ///
///     with the stack  ///////
///////////////////////////////

function generatePrettyErrorObj(data) {
  return generateErrorObj(data).split('\n').map( line => '      ' + line).join('\n')
}

function generateErrorObj(data) {
  let tmpString = JSON.stringify(data, replacer, 2)
  let result = tmpString.replace(/"{/g, '{ ').replace(/}"/g, ' }').replace(/\\/g, '').replace(/(:|,)(.)/g, '$1 $2')
  return result
}

function replacer(key, value) {
  if (isNaN(key) || key === '') return value
  return JSON.stringify(value)
}