class Foo
  def initialize json
    @foo = json['foo']
  end
end

Foo.class_eval {
  define_method :get_foo do
    puts @foo
  end
}

Foo.new({'foo' => 3}).get_foo