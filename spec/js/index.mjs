import { constructMessage } from '../../lib/js/make_message.mjs'
import validateMessage from '../../lib/js/type_checker.mjs'

let data = {
  id: 'hjkg3jke2e',
  age: 20,
  friends: [
    {
      name: 'annie',
      age: 1,
      something: 'jhsdkhfbsdk'
    },
    {
      name: 'gay',
      age: 8,
      something: 'jhsdkhfbsdk'
    },
    {
      name: 'annie',
      age: 8,
      something: 'hihi'
    },
    {
      name: 'hi',
      age: 3,
      something: 'jhsdkhfbsdk'
    }
  ]
}

let exampleJsonProtocol = {
  line: 'str',
  add_client_msg: {
    type: 'const:add_client',
    id: 'str',
    age: 'num',
    friends: {list: {
      name: 'str',
      age: 9,
      something: 'line'
    }}
  }
}

let msg = constructMessage(data, exampleJsonProtocol['add_client_msg'])
let result = validateMessage(msg, 'add_client_msg', exampleJsonProtocol)

console.log(msg);
console.log(result);